'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('ListadoCtrl', ['$scope', 'usSpinnerService', function($scope, usSpinnerService) { //function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.options = {
      'resource': 'reporte',
      'title': 'Reporte',
      'columns': [
      	{'title': 'Año' , 'data': 'anio'},
		{'title': 'Semana' , 'data' : 'semana'},
		{'title': 'Sexo' , 'data' : 'sexo'},
		{'title': 'ID' , 'data' : 'id', 'visible': false},
		{'title': 'Cantidad' , 'data' : 'cantidad'},
		{'title': 'Mes' , 'data' : 'mes'},
		{'title': 'Día' , 'data' : 'dia'},
		{'title': 'Region' , 'data' : 'region'},
		{'title': 'País' , 'data' : 'pais'},
		{'title': 'Cod. Nivel Adm. 1' , 'data' : 'adm1Codigo', 'visible': false},
		{'title': 'Nivel Adm. 1' , 'data' : 'adm1Nombre'},
		{'title': 'Cod. Nivel Adm. 2' , 'data' : 'adm2Codigo', 'visible': false},
		{'title': 'Nivel Adm. 2' , 'data' : 'adm2Nombre'},
		{'title': 'Cod. Nivel Adm. 3' , 'data' : 'adm3Codigo', 'visible': false},
		{'title': 'Nivel Adm. 3' , 'data': 'adm3Nombre'},
		{'title': 'Sexo' , 'data' : 'sexo'},
		{'title': 'Grupo Edad' , 'data' : 'grupo_edad'},
		{'title': 'Estado Final' , 'data' : 'estado_final'},
		{'title': 'Clasif. clínica' , 'data' : 'clasificacion_clinica'},
		{'title': 'Serotipo' , 'data' : 'serotipo'},
		{'title': 'Fuente' , 'data' : 'fuente', 'visible': false},
		{'title': 'Origen' , 'data' : 'origen', 'visible': false}
      ]
    };

	$scope.startSpinTable = function(){
        usSpinnerService.spin('spinner-table');
    }
    $scope.stopSpinTable = function(){
        usSpinnerService.stop('spinner-table');
    }

    /******* Dowload with filters *******/

    var urlTemplate = window.basePath + "/denguemaps-server/rest/reporte/lista?";
    var tableId = ".table"; //find table by class

  	/* Apply the request for JSON download */
  	$("#btn-descarga-json").on(
      'click',
      function() {
        var oTable = $(tableId).dataTable();
        var oParams = oTable.oApi
            ._fnAjaxParameters(oTable.fnSettings());
        var columnFilters = oParams.columns
        var tieneFiltro = false;
        _.each(columnFilters, function(filter) {
          if (filter.search.value != "")
                tieneFiltro = true;
        });
        if (tieneFiltro) {
        	$scope.startSpinTable();
          	$.ajax({
	            type : 'GET',
	            url : urlTemplate + '?' + $.param(oParams),
	            beforeSend : function(xhr) {
	              xhr.setRequestHeader('Accept', 'application/csv');
	            },
	            success : function(response) {
	              var nameAux = urlTemplate.split("/");
	              var name = nameAux[nameAux.length - 2]; // resource is name
	              download(response, name + ".json", "text/json");
	              $scope.stopSpinTable();
	            },
	            error : function() {
	              console.log("error");
	              $scope.stopSpinTable();
	              alert("Ha ocurrido un error.");
	            }
          	});
        } else {
          alert("Debe indicar al menos un filtro para poder realizar la descarga.");
        }

      });


    /* Apply the request for CSV download */
  	$("#btn-descarga-csv").on(
      'click',
      function() {

        var oTable = $(tableId).dataTable();
        var oParams = oTable.oApi
            ._fnAjaxParameters(oTable.fnSettings());
        var columnFilters = oParams.columns
        var tieneFiltro = false;
        _.each(columnFilters, function(filter) {
          if (filter.search.value != "")
                tieneFiltro = true;
        });
        if (tieneFiltro) {
        	$scope.startSpinTable();
          	$.ajax({
	            type : 'GET',
	            url : urlTemplate + '?' + $.param(oParams),
	            beforeSend : function(xhr) {
	              xhr.setRequestHeader('Accept', 'application/csv');
	            },
	            success : function(response) {
	              var nameAux = urlTemplate.split("/");
	              var name = nameAux[nameAux.length - 2]; //resource is name
	              var JSONData = response;
	              var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
	              var CSV = '';
	              var row = "";
	              // This loop will extract the label from 1st index of on array
	              for ( var index in arrData[0]) {
	                  // Now convert each value to string and comma-seprated
	                  row += index + ',';
	              }
	              row = row.slice(0, -1);
	              // append Label row with line break
	              CSV += row + '\r\n';
	              // 1st loop is to extract each row
	              for (var i = 0; i < arrData.length; i++) {
	                  var row = "";

	                  // 2nd loop will extract each column and convert it in string
	                  // comma-seprated
	                  for ( var index in arrData[i]) {
	                      row += '"' + arrData[i][index] + '",';
	                  }
	                  row.slice(0, row.length - 1);
	                  // add a line break after each row
	                  CSV += row + '\r\n';
	              }

	              if (CSV == '') {
	                  alert("Invalid data");
	                  return;
	              }
	              download(CSV, name + ".csv", "text/csv");
	              $scope.stopSpinTable();
	            },
	            error : function() {
	              console.log("error");
	              $scope.stopSpinTable();
	              alert("Ha ocurrido un error.");
	            }
          	});
        } else {
          alert("Debe indicar al menos un filtro para poder realizar la descarga.");
        }
      });

  }]);
