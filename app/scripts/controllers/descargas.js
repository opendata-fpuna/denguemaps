'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:DescargasCtrl
 * @description
 * # DescargasCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('DescargasCtrl', ['$scope', 'usSpinnerService', function($scope, usSpinnerService) {
  	var urlBase = window.basePath + "/denguemaps-server/rest/reporte/";

  	$scope.startSpinDropdown = function(){
        usSpinnerService.spin('spinner-dropdown');
    }
    $scope.stopSpinDropdown = function(){
        usSpinnerService.stop('spinner-dropdown');
    }

  	$scope.descargar = function (anio,formato) {
  		if (formato == 'CSV') {
  			descargarCSV(anio);
  		} else {
  			descargarJSON(anio);
  		}
  	};

    function descargarCSV (anio) {
	    $scope.startSpinDropdown();
	    var data;
	    $.ajax({
	        url: urlBase + "descarga/" + anio,
	        type:"get",
	        success: function(response) {
	            var JSONData = response;
	            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
	            var CSV = '';
	            var row = "";
	            // This loop will extract the label from 1st index of on array
	            for ( var index in arrData[0]) {
	                // Now convert each value to string and comma-seprated
	                row += index + ',';
	            }
	            row = row.slice(0, -1);
	            // append Label row with line break
	            CSV += row + '\r\n';
	            // 1st loop is to extract each row
	            for (var i = 0; i < arrData.length; i++) {
	                var row = "";

	                // 2nd loop will extract each column and convert it in string
	                // comma-seprated
	                for ( var index in arrData[i]) {
	                    row += '"' + arrData[i][index] + '",';
	                }
	                row.slice(0, row.length - 1);
	                // add a line break after each row
	                CSV += row + '\r\n';
	            }

	            if (CSV == '') {
	                alert("Invalid data");
	                return;
	            }
	            $scope.stopSpinDropdown();
	            download(CSV, "notificaciones.csv", "text/csv");
	        },
	        error: function(xhr) {
	        	$scope.stopSpinDropdown();
	            console.log('error');
	        }
    	});
	};

	function descargarJSON (anio) {
	    $scope.startSpinDropdown();
	    var data;
	    $.ajax({
	        url: urlBase + "descarga/" + anio,
	        type:"get",
	        success: function(response) {
	            data = response;
	            $scope.stopSpinDropdown();
	            download(JSON.stringify(data, null, 4), "notificaciones.json", "application/json");
	        },
	        error: function(xhr) {
	        	$scope.stopSpinDropdown();
	            console.log('error');
	        }
	    });
	}
  }]);
