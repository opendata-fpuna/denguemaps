'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('MainCtrl', function ($scope, $rootScope) {
       
       /* $scope.isActiveTab = function (tab) {
          console.log("dentro de isActiveTab");
          if ($rootScope.tab == tab) {
            return true;
          } else if (!$rootScope.tab && tab=='map5') {
            return true;
          } else {
            return false;
          }
        }

        //$rootScope.tab.on('change', setActiveTab);

        var MECONF = MECONF || {};
        MECONF.tilesLoaded = false;

        MECONF.LAYERS = function () {
            var mapbox = L.tileLayer(
                    'http://api.tiles.mapbox.com/v4/rparra.jmk7g7ep/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg');
            var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {minZoom: 3});
            var gglHybrid = new L.Google('HYBRID');
            var gglRoadmap = new L.Google('ROADMAP');
            return {
                MAPBOX: mapbox,
                OPEN_STREET_MAPS: osm,
                GOOGLE_HYBRID: gglHybrid,
                GOOGLE_ROADMAP: gglRoadmap
            }
        };

        var finishedLoading = function() {
          if(tilesLoaded){
            $(".spinner").remove();
            MECONF.tilesLoaded = false;
          }

        };

        var startLoading = function() {
          var spinner = new Spinner({
              color: "#ffb885",
              radius: 10,
              width: 5,
              length: 10,
              top: '92%',
              left: '98%'
          }).spin();
          $("#map").append(spinner.el);
        };

        var setup_gmaps = function() {
          google.maps.event.addListenerOnce(this._google, 'tilesloaded', tilesLoaded);
        };

        var tilesLoaded = function(){
          MECONF.tilesLoaded = true;
          finishedLoading();
        }

      startLoading();

      L.mapbox.accessToken = 'pk.eyJ1IjoicnBhcnJhIiwiYSI6IkEzVklSMm8ifQ.a9trB68u6h4kWVDDfVsJSg';
      var layers = MECONF.LAYERS();
      var mapbox = layers.MAPBOX.on('load', tilesLoaded);
      var osm = layers.OPEN_STREET_MAPS.on('load', tilesLoaded);

      var gglHybrid = layers.GOOGLE_HYBRID.on('MapObjectInitialized', setup_gmaps);
      var gglRoadmap = layers.GOOGLE_ROADMAP.on('MapObjectInitialized', setup_gmaps);

      /* Layer para probar opacidad */
      /*var historic_seattle = new L.tileLayer.wms('http://demo.lizardtech.com/lizardtech/iserv/ows', {
              layers: 'Seattle1890',
              maxZoom: 18,
              format: 'image/png',
              transparent: true
          });*/

     /* var map = L.map('map', {maxZoom: 18, minZoom: 3, worldCopyJump: true, attributionControl: false, zoomControl: false})
            .setView([-24, -57.189], 7)
            //.setView([47.59, -122.30], 12)
            .on('baselayerchange', startLoading);
      new L.Control.Zoom({ position: 'topright' }).addTo(map);

      var baseMaps = {
        'Calles OpenStreetMap': osm,
        'Terreno': mapbox,
        'Satélite': gglHybrid,
        'Calles Google Maps': gglRoadmap
      };

      map.addLayer(gglRoadmap);

      //var sidebar = L.control.sidebar('sidebar').addTo(map);

      MECONF.map = map;
      $rootScope.MAP = MECONF.map;

      console.log(MECONF.map);

      console.log("MAIN CONTROLLER");*/
});
