'use strict';

angular.module('denguemapsApp').factory('CORSService', function () {
        var cors = {};

        // Create the XHR object.
        cors.createCORSRequest = function (method, url) {
          console.log("createCORSRequest");
          var xhr = new XMLHttpRequest();
          if ("withCredentials" in xhr) {
            // XHR for Chrome/Firefox/Opera/Safari.
            xhr.open(method, url, true);
          } else if (typeof XDomainRequest != "undefined") {
            // XDomainRequest for IE.
            xhr = new XDomainRequest();
            xhr.open(method, url);
          } else {
            // CORS not supported.
            xhr = null;
          }
          return xhr;
        }

        // Make the actual CORS request.
        cors.makeCorsRequest = function (method, url, data, callback) {
          console.log("makeCorsRequest");
          // All HTML5 Rocks properties support CORS.

          var xhr = cors.createCORSRequest(method, url);
          if (!xhr) {
            console.log('CORS not supported');
            return;
          }
          // Response handlers.
          xhr.onload = function (response) {
            console.log('Response from CORS request to ' + url);
            callback(xhr);
          }

          xhr.onerror = function() {
            console.log('Woops, there was an error making the request.');
            callback(xhr);
          };

          xhr.send();
        }

        return cors;
    })