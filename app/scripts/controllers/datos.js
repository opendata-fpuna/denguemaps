'use strict';

/**
 * @ngdoc function
 * @name denguemapsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the denguemapsApp
 */
angular.module('denguemapsApp')
  .controller('DatosCtrl', function ($scope, fileUpload, toastr) {

	$scope.uploadFile = function(){
		if ($scope.myFile) {
			console.log($scope.myFile);
	        var file = $scope.myFile;
	        //console.log('file is ' + JSON.stringify(file));
	        var uploadUrl = window.basePath + "/denguemaps-server/rest/file/upload";
	        //var uploadUrl = "http://localhost:8088/municipalidad-app-server/rest/movil/notificacion?latitud=-56.4435053&longitud=-25.7811414&idtipo=1&descripcion=test&identificador=xxx";
	        var result = fileUpload.uploadFileToUrl(file, uploadUrl)
	        	.then(function(data){
	        		console.log(data);
					console.log(data.file);
					switch(data.file){
						case "error_interno":
							$scope.msgUpload = false;
			            	toastr.error('Error interno del sistema','Error');
			            	break;
						case "formato_invalido":
							$scope.msgUpload = false;
			            	toastr.error('El formato del archivo no es válido','Error');
			            	break;
			            case "contenido_de_filas_invalido":
			            	$scope.msgUpload = false;
							toastr.error('El contenido del archivo no es válido','Error');
							break;
						case "estructura_de_columnas_invalida":
							$scope.msgUpload = false;
							toastr.error('La estructura de columnas del archivo no es válida','Error');
							break;
						default:
							$scope.msgUpload = true;
			            	toastr.success('La carga de datos se ha realizado exitosamente');
			            	break;
					}
					return data;
				})
		} else {
			toastr.warning("No se seleccionó ningun archivo.");
		}

    };

    $("#filestyle-7").on('change', function (){
    	var value = $("#filestyle-7").val();
    	var splitValue = value.split('\\');
    	$("#selected-file-value").val(splitValue[splitValue.length - 1]);
    })

  });